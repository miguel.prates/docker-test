FROM alpine:latest
#FROM openjdk:8-jdk-slim
ENV PATH=/app:$PATH
COPY texto.sh /app/

WORKDIR /app
ENTRYPOINT ["./texto.sh"]